import React, { Component } from "react";
import styled from "styled-components";

import Wax from "../Wax";
import WaxSchema from "../config/classes/WaxSchema";
import WaxKeys from "../config/classes/WaxKeys";

import plugins from "../config/plugins";
import MainMenuBar from "../components/menu/MainMenuBar";

import {
  tableNodes,
  columnResizing,
  tableEditing,
  goToNextCell
} from "prosemirror-tables";

const WaxContainer = styled.div`
  ${"" /* style override*/};
`;

const MenuBarStyled = styled(MainMenuBar)`
  ${"" /* main menu style override*/};
`;
const LeftMenuBarContainer = styled.div`
  position: absolute;
  top: 50px;
  width: 155px;
  height: 100%;
  border-right: 1px solid #eeeeee;
`;
const LeftMenuBarStyled = styled(MainMenuBar)`
  ${"" /* main menu style override*/};
  overflow: auto;
  height: 95%;
  button {
    margin-top: 10px;
  }
  &:before {
    content: "Block Styles";
    font-weight: strong;
    font-size: 18px;
    margin-left: 10px;
    margin-bottom: 20px;
    border-bottom: 1px solid #000;
  }
  button {
    display: block;
  }
`;

const MainEditor = styled.div`
  ${"" /* style override*/};
  ${"" /* background: rgba(200, 200, 200, 0.1);*/};
`;

// add your customPlugins

// Extend the Schema
// const config = {
//   nodes: tableNodes({
//     tableGroup: "block",
//     cellContent: "block+"
//   }),
//   marks: {}
// };

// const schema = WaxSchema(config);
// const schema = WaxSchema();

// const shortCuts = {
//   Tab: goToNextCell(1),
//   "Shift-Tab": goToNextCell(-1)
// };

// const keys = new WaxKeys({ schema: schema, shortCuts: shortCuts });

// const customPlugins = [columnResizing(), tableEditing()];
// plugins.push(...customPlugins);

// const options = {
//   schema,
//   plugins,
//   keys
// };

const fileUpload = file => {
  // returns a promise with the
  //actual file path
};

const onChange = () => {
  console.log("configurable on change");
};

const onBlur = () => {
  console.log("on Blur");
};

const menuItems = [
  "undo",
  "redo",
  "strong",
  "em",
  "subscript",
  "superscript",
  "underline",
  "strikethrough",
  "link",
  "code",
  "source",
  "small_caps",
  "ordered_list",
  "bullet_list",
  "lift",
  "join_up",
  "link",
  "image",
  "table",
  "tableDropDownOptions",
  "codeBlock",
  "footnote",
  "selectParentNode"
];

const leftMenuItems = [
  "heading1",
  "subtitle",
  "epigraph",
  "heading2",
  "heading3",
  "heading4",
  "heading5",
  "plain",
  "quote",
  "special",
  "important",
  "recommended",
  "caution",
  "tip",
  "imageTitle",
  "imageCaption"
];

class Configurable extends Component {
  render() {
    return (
      <WaxContainer className="wax-container">
        <Wax
          autoFocus
          onChange={values => true}
          // readonly
          // options={options}
          theme={"booksprint"}
          onBlur={onBlur}
          value=""
          fileUpload={fileUpload}
          // placeholder={"Type Something..."}
          renderLayout={({ editor, ...props }) => (
            <React.Fragment>
              <MenuBarStyled
                className="main-toolbar"
                menuItems={menuItems}
                {...props}
              />
              <LeftMenuBarContainer>
                <LeftMenuBarStyled
                  className="left-toolbar"
                  menuItems={leftMenuItems}
                  {...props}
                />
              </LeftMenuBarContainer>
              <MainEditor className="main-editor">{editor}</MainEditor>
            </React.Fragment>
          )}
        />
      </WaxContainer>
    );
  }
}

export default Configurable;
