import React, { Component } from "react";

import Wax from "../Wax";

const fileUpload = () => {
  console.log("default");
};

const onChange = () => {
  console.log("default on change");
};

class Default extends Component {
  render() {
    return (
      <Wax
        autoFocus
        fileUpload={fileUpload}
        onChange={onChange}
        value={""}
        theme={"default"}
        placeholder={"Type Something..."}
      />
    );
  }
}

export default Default;
